const dotenv = require('dotenv');
const mongoose = require('mongoose');
const fs = require('fs');
const Tour = require('../../models/tourModel');

dotenv.config({ path: './config.env' });
// console.log(process.env);

const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('CONNECTION SUCESSFUL');
  })
  .catch((err) => console.log(err.message));

const tours = JSON.parse(
  fs.readFileSync(`${__dirname}/tours-simple.json`, 'utf-8')
);

//import data from
const importData = async () => {
  try {
    await Tour.create(tours);
    console.log('data sucessully loaded');
    process.exit(0);
  } catch (error) {
    console.log(error);
  }
};

//delete all data from collection

const deleteData = async () => {
  try {
    await Tour.deleteMany();
    console.log('data sucessully deleted');
    process.exit(0);
  } catch (error) {
    console.log(error);
  }
};

if (process.argv[2] === '--import') {
  importData();
} else if (process.argv[2] === '--delete') {
  deleteData();
}
