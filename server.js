const dotenv = require('dotenv');
const mongoose = require('mongoose');
dotenv.config({ path: './config.env' });

const app = require('./app');

// console.log(process.env);

const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('CONNECTION SUCESSFUL');
  })
  .catch((err) => console.log(err.message));

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  // console.log('app running on port', PORT);
});
