const express = require('express');
const morgan = require('morgan');

const {
  getAllTours,
  getTour,
  updateTour,
  createTour,
  deleteTour,
} = require('./controllers/tourController');

const {
  getAllUsers,
  createUser,
  getUser,
  updateUser,
  deleteUser,
} = require('./controllers/userController');

//config
const app = express();

//middlewaare
if (process.env.NODE_ENV === 'development') app.use(morgan('dev'));

app.use(express.json());
app.use(express.static(`${__dirname}/public`));

app.use((req, res, next) => {
  // console.log('my middleware.....................');
  req.requestTime = new Date().toISOString();
  next();
});

//ROUTES
const tourRouter = express.Router();
app.use('/api/v1/tours', tourRouter);

tourRouter.route('/').get(getAllTours).post(createTour);

tourRouter.route('/:id').get(getTour).patch(updateTour).delete(deleteTour);

//userRouter
const userRouter = express.Router();
app.use('/api/v1/users', userRouter);

userRouter.route('/').get(getAllUsers).post(createUser);

userRouter.route('/:id').get(getUser).patch(updateUser).delete(deleteUser);
//update
// app.patch('/api/v1/tours/:id', updateTour);

// //deltees
// app.delete('/api/v1/tours/:id', deleteTour);

//server

module.exports = app;
